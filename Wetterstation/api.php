<?php
require_once('models/Measurement.php');
require_once('models/Station.php');
require_once('controllers/RESTController.php');
require_once('controllers/StationRESTController.php');
require_once('controllers/MeasurementRESTController.php');
require_once('controllers/MeasurementController.php');

/*
 * Dieses Skript erledigt das Routing. Abhängig von der URL wird der richtige Teil der Anwendung geladen
 */
//Falls der GET-Parameter r existiert, soll geroutet werden
if (isset($_GET['r']))
{
    $route = explode('/',$_GET['r']);
    //var_dump($route);
}
else{
    //Die Standardroute, falls r nicht gesetzt ist.
    $route = ['stations'];
}

//Hole den Namen des Controllers aus der Route
if (sizeof($route) > 0)
{
    $controller = $route[0];
}

//Wenn Stations gefragt sind...

if ($controller == 'stations')
{
    try {
        (new StationRESTController())->handleRequest();
    }
    catch(Exception $e)
    {
        RESTController::responseHelper($e->getMessage(), $e->getCode());
    }
}
elseif ($controller == 'measurement')
{
    try {
        (new MeasurementRESTController())->handleRequest();
    }
    catch(Exception $e)
    {
        RESTController::responseHelper($e->getMessage(), $e->getCode());
    }
}
else{
    RESTController::responseHelper('REST-Controller '. $controller. ' not found', 404);
}