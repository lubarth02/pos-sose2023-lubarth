<?php
require_once('RESTController.php');
require_once('models/Station.php');
require_once('models/Measurement.php');

class StationRESTController extends RESTController
{

    public function handleRequest()
    {
        switch ($this->method) {
            case 'GET':
                $this->handleGETRequest();
                break;
            case 'POST':
                $this->handlePOSTRequest();
                break;
            case 'PUT':
                $this->handlePUTRequest();
                break;
            case 'DELETE':
                $this->handleDELETERequest();
                break;
            default:
                $this->response('StationRESTController: Method not allowed', 405);
                break;
        }
    }

    /**
     * Behandelt alle GET-Requests auf den Endpoint /stations/
     */
    public function handleGETRequest()
    {
        // Wenn keine Argumente-> Einfach alle Stations auslesen
        if ($this->verb == null && sizeof($this->args) == 0) {
            $model = Station::getAll();
            $this->response($model, 200);
        }
        //Wenn eine ID vorhanden ist
        elseif ($this->verb == null && sizeof($this->args) == 1) {
            $model  = Station::get($this->args[0]);
            $this->response($model, 200);
        }
        //Wenn die Messwerte für eine Station zurückgeliefert werden sollen
        else if (sizeof($this->args) == 2 && $this->args[1] == 'measurement') {
            $model = Measurement::getAllByStation($this->args[0]);
            $this->response($model, 200);
        } else {
            $this->response("Bad request", 400);
        }
    }

    /**
     * Behandelt alle POST-Requests auf den Endpoint /stations/
     */
    public function handlePOSTRequest()
    {
        $model = new Station();
        $model->setName($this->getDataOrNull('name'));
        $model->setAltitude($this->getDataOrNull('altitude'));
        $model->setLocation($this->getDataOrNull('location'));

        if ($model->save()) {
            $this->response("OK", 201);
        } else {
            $this->response($model->getErrors(), 400);
        }
    }


    /**
     * Behandelt alle PUT Requests auf den Endpoint /stations/
     */
    private function handlePUTRequest()
    {
        //Wenn kein Verb übergeben wurde und genau ein Argument (die ID) vorliegt
        if ($this->verb == null && sizeof($this->args) == 1) {

            //Hole das Objekt vom Model
            $model = Station::get($this->args[0]);

            if ($model == null) {
                $this->response("Not found", 404);
            }
            //übergib die Daten aus dem PUT-Request an das Model
            else {
                $model->setName($this->getDataOrNull('name'));
                $model->setAltitude($this->getDataOrNull('altitude'));
                $model->setLocation($this->getDataOrNull('location'));

                //Speichere die Änderungen in der DB
                if ($model->save()) {
                    $this->response("OK");
                } else {
                    $this->response($model->getErrors(), 400);
                }
            }
        } else {
            $this->response("Not Found", 404);
        }
    }


    /**
     * behandelt den DELETE Request auf /stations/
     */
    private function handleDELETERequest()
    {
        if ($this->verb == null && sizeof($this->args) == 1) {
            Station::delete($this->args[0]);
            $this->response("OK");
        } else {
            $this->response("Not Found", 404);
        }
    }
}
