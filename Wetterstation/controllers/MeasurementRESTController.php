<?php
require_once('RESTController.php');
require_once('models/Measurement.php');
class MeasurementRESTController extends RESTController
{

    public function handleRequest()
    {
        switch ($this->method) {
            case 'GET':
                $this->handleGETRequest();
                break;
            case 'POST':
                $this->handlePOSTRequest();
                break;
            case 'PUT':
                $this->handlePUTRequest();
                break;
            case 'DELETE':
                $this->handleDELETERequest();
                break;
            default:
                $this->response('Measurement RESTController: Method not allowed', 405);
                break;
        }
    }
    /**
     * behandelt den GET Request
     */
    public function handleGETRequest()
    {
        // Wenn keine ID angegeben wurde
        if ($this->verb == null && sizeof($this->args) == 0) {
            $model = Measurement::getAll();
            $this->response($model, 200);
        }

        // Wenn eine ID übergeben wird
        if ($this->verb == null && sizeof($this->args) == 1) {
            $model = Measurement::get($this->args[0]);
            $this->response($model, 200);
        }
    }

    /**
     * behandelt den POST Request
     */
    public function handlePOSTRequest()
    {
        $model = new Measurement();
        $model->setRain($this->getDataOrNull("rain"));
        $model->setStationId($this->getDataOrNull("station"));
        $model->setTemperature($this->getDataOrNull("temperature"));
        $model->setTime($this->getDataOrNull("time"));
        if ($model->save()) {
            $this->response($model, 201);
        } else {
            $this->response($model->getErrors(), 400);
        }
    }

    /**
     * behandelt den PUT Request
     */
    public function handlePUTRequest()
    {
        //Wenn kein Verb übergeben wurde und genau ein Argument (die ID) vorliegt
        if ($this->verb == null && sizeof($this->args) == 1) {

            //Hole das Objekt vom Model
            $model = Measurement::get($this->args[0]);

            if ($model == null) {
                $this->response("Not found3", 404);
            }
            //übergib die Daten aus dem PUT-Request an das Model
            else {
                $model->setRain($this->getDataOrNull("rain"));
                $model->setStationId($this->getDataOrNull("station"));
                $model->setTemperature($this->getDataOrNull("temperature"));
                $model->setTime($this->getDataOrNull("time"));

                //Speichere die Änderungen in der DB
                if ($model->save()) {
                    $this->response("OK");
                } else {
                    $this->response($model->getErrors(), 400);
                }
            }
        } else {
            $this->response("Not Found", 404);
        }
    }
    /**
     * behandelt den DELETE Request
     */
    public function handleDELETERequest()
    {
        if ($this->verb == null && sizeof($this->args) == 1) {
            Station::delete($this->args[0]);
            $this->response("OK");
        } else {
            $this->response("Not Found", 404);
        }
    }
}
