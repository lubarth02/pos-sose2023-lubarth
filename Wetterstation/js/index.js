document.addEventListener('DOMContentLoaded', function () {

    let button = document.getElementById('btnSearch');
    let select = document.getElementById('dropdown');

    makeAjaxRequest();

    button.addEventListener('click', function () {
        makeAjaxRequest();
    });

    select.addEventListener('change', function () {
        makeAjaxRequest();
    });

    // Alternative zu AJAX
    // async function getAllMeasurements(){
    //     let response = await fetch("api/measurement");
    //     let jsonData = await jsonData.json();
    //     console.log(jsonData);
    // }

    //getAllMeasurements();

    function makeAjaxRequest() {
        // AJAX Request an die API
        let ajax = new XMLHttpRequest();
        ajax.open('GET', 'api/stations/' + select.value + '/measurement', true);
        ajax.responseType = 'json';
        ajax.onload = function () {
            loadChart(ajax.response);
            loadTable(ajax.response);
        }
        ajax.send();
    }

    function loadChart(data) {

        // wenn schon ein Chart vorhanden dann wird es gelöscht
        let oldCart = Chart.getChart('chart');
        if (oldCart != undefined) {
            oldCart.destroy();
        }

        let zeitstempel = [];
        let temperatur = [];
        let regen = [];

        // jedes Objekt durchlaufen und Zeit, Temperatur und Regen auslesen
        data.forEach(element => {
            zeitstempel.push(element.time);
            temperatur.push(element.temperature);
            regen.push(element.rain);
        });

        var ctx = document.getElementById('chart');
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
                labels: zeitstempel,
                datasets: [{
                    label: "Temperatur [°C]",
                    data: temperatur,
                    borderColor: 'rgb(75, 192, 192)',
                    backgroundColor: 'rgb(75, 192, 192)',
                    fill: false,
                    tension: 0
                },
                {
                    label: "Regenmenge [ml]",
                    data: regen,
                    borderColor: 'rgb(0, 0, 255)',
                    backgroundColor: 'rgb(0, 0, 255)',
                    fill: false,
                    tension: 0
                }]
            },

            // Configuration options go here
            options: {
                scales: {
                    yAxes: [{
                        type: 'linear',
                        position: 'left',
                        ticks: {
                            beginAtZero: true,
                            max: 25
                        }
                    }]
                }
            }
        });

    }

    function loadTable(data) {
        let zeilenHTML = '';
        let tablebody = document.getElementById('measurements');
        
       //// jedes Objekt durchlaufen und Zeit, Temperatur und Regen auslesen und der Tabelle hinzufügen
        data.forEach(element => {
            zeilenHTML += "<tr>";
            zeilenHTML += "<td>";
            zeilenHTML += element.time;
            zeilenHTML += "</td>";
            zeilenHTML += "<td>";
            zeilenHTML += element.temperature;
            zeilenHTML += "</td>";
            zeilenHTML += "<td>";
            zeilenHTML += element.rain;
            zeilenHTML += "</td>";
            zeilenHTML += "<tr>";
        });

        tablebody.innerHTML = zeilenHTML;
    }
});