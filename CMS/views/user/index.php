<?php

require_once("../../models/User.php");

if (!User::isLoggedIn()) {
    header("Location: ../helper/login.php");
} else {

    $users = User::getAll();
}

?>


<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

    <?php
    $pathToUsers = "index.php";
    $pathToArticles = "../article/index.php";
    $pathToIdex = "../../index.php"; 
    include "../helper/navbar.php";
    ?>

    <div class="container">
        <div class="row">
            <h2>Benutzer</h2>
        </div>
        <div class="row">
            <p>
                <a href="register.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
            </p>

            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Nummer</th>
                        <th>Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($users as $user) {
                        echo "<tr>";
                        echo "<td>" . $user->getUid() .  "</td>";
                        echo "<td>" . $user->getUname() .  "</td>";
                        $id = $user->getUid();
                        echo " <td><a class='btn btn-info' href='view.php?id=$id'><span class='glyphicon glyphicon-eye-open'></span></a>&nbsp;
                    <a class='btn btn-primary' href='update.php?id=$id'><span class='glyphicon glyphicon-pencil'></span></a>&nbsp;
                    <a class='btn btn-danger' href='delete.php?id=$id'><span class='glyphicon glyphicon-remove'></span></a></td>";
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div> <!-- /container -->
</body>

</html>