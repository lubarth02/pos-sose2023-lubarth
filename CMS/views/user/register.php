<?php
$pathToUsers = "index.php";
$pathToArticles = "../article/index.php";
$pathToIdex = "../../index.php"; 
require_once("../../models/User.php");

$salt = "PHP";
$user = new User();

if (isset($_POST['register'])) {
    if (isset($_POST['benutzername'])) {
        $user->setUname($_POST['benutzername']);
    }
    if (isset($_POST['passwort'])) {
        $passwort = $_POST['passwort'];
        $pwhash = hash('sha512', $passwort . $salt);
        $user->setUpwhash($pwhash);
    }
    if ($user->getUname() == null | $user->getUpwhash() == null) {
        echo "<div class='alert alert-danger'> <p> Die Daten sind ungültig!</p>";
        echo "<ul>";
        if (isset($_SESSION['uname'])) {
            echo "<li>" . $_SESSION['uname'] . "</li>";
        }
        if (isset($_SESSION['upwhash'])) {
            echo "<li>" . $_SESSION['upwhash'] . "</li>";
        }
    } else {
        header("Location: index.php");
        $user->create();
    }
}

?>

<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

    <?php
    $pathToUsers = "index.php";
    $pathToArticles = "../article/index.php";
    $pathToIdex = "../../index.php"; 
    include "../helper/navbar.php";

    require_once("../../models/Article.php");
    require_once("../../models/User.php");
    ?>
    <div class="container m-3">
        <form class="form-signin" action="register.php" method="post">
            <h2 class="form-signin-heading">Bitte registrieren</h2>
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="form-group required">
                        <label class="control-label">Benutzername *</label>
                        <input type="text" class="form-control" name="benutzername" maxlength="45" value="">
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group required">
                        <label class="control-label">Passwort *</label>
                        <input type="password" class="form-control" name="passwort" maxlength="45" value="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <button class="btn btn-lg btn-primary btn-block" name="register" type="submit">registrieren</button>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a class="btn btn-lg btn-primary btn-block" href="login.php" type="button">login</a>
                </div>
            </div>
        </form>
    </div>
</body>

</html>