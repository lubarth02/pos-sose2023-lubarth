<?php

require_once("../../models/User.php");

if (!User::isLoggedIn()) {
    header("Location: ../helper/login.php");
} else {
    $id = '';
    $user = new User();
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $user = User::get($id);
    } else {
        header('Location: index.php');
    }
}
?>

<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

    <?php
    $pathToUsers = "index.php";
    $pathToArticles = "../article/index.php";
    $pathToIdex = "../../index.php"; 
    include "../helper/navbar.php";
    ?>

    <div class="container">
        <h2>Benutzer anzeigen</h2>

        <p>
            <?php
            echo "<a class='btn btn-primary' href='update.php?id=$id'>Aktualisieren</a>";
            echo "<a class='btn btn-danger' href='delete.php?id=$id'>Löschen</a>";
            echo "<a class='btn btn-default' href='index.php'>Zurück</a>";
            ?>
        </p>

        <table class="table table-striped table-bordered detail-view">
            <tbody>
                <tr>
                    <th>Nummer</th>
                    <?php echo "<td>" . $user->getUid() . "</td>"; ?>
                </tr>
                <tr>
                    <th>Name</th>
                    <?php echo "<td>" . $user->getUname() . "</td>"; ?>
                </tr>

            </tbody>
        </table>
    </div> <!-- /container -->
</body>

</html>