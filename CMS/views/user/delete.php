<?php

require_once("../../models/User.php");

$id = "";
if (isset($_GET['id'])) {
    $id = $_GET['id'];
}

if (isset($_POST['deleteUser'])) {
    if(isset($_POST['id'])){
        User::delete($_POST['id']);
        header("Location: index.php");
    }   
}
?>

<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

    <?php
    $pathToUsers = "index.php";
    $pathToArticles = "../article/index.php";
    $pathToIdex = "../../index.php"; 
    include "../helper/navbar.php";
    ?>

    <div class="container">
        <h2>Benutzer löschen</h2>

        <form class="form-horizontal" action="delete.php?id=$id" method="post">
            <?php echo "<input type='hidden' name='id' value='$id'/>"; ?>
            <p class="alert alert-error">Wollen Sie den Benutzer wirklich löschen?</p>
            <div class="form-actions">
                <button type="submit" name="deleteUser" class="btn btn-danger">Löschen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->
</body>

</html>