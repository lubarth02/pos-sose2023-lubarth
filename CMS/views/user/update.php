<?php
require_once("../../models/User.php");

$user = new User();
$salt = "PHP";
$passwort = "";

if (isset($_GET['id'])) {
    $user = User::get($_GET['id']);
}
if (isset($_POST['userAktualisieren'])) {
    if (isset($_POST['name'])) {
        $user->setUname($_POST['name']);
    }
    if (isset($_POST['passwort'])) {
        $passwort = $_POST['passwort'];
        $pwhash = hash('sha512', $passwort . $salt);
        $user->setUpwhash($pwhash);
    }

    if ($user->getUname() == null | $user->getUpwhash() == null) {
        echo "<div class='alert alert-danger'> <p> Die Daten sind ungültig!</p>";
        echo "<ul>";
        if (isset($_SESSION['uname'])) {
            echo "<li>" . $_SESSION['uname'] . "</li>";
        }
        if (isset($_SESSION['upwhash'])) {
            echo "<li>" . $_SESSION['upwhash'] . "</li>";
        }
    } else {
        $user->update();
        header("Location: index.php");
    }
}
?>

<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

    <?php
    $pathToUsers = "index.php";
    $pathToArticles = "../article/index.php";
    $pathToIdex = "../../index.php"; 
    include "../helper/navbar.php";
    ?>

    <div class="container">
        <div class="row">
            <h2>Benutzer bearbeiten</h2>
        </div>

        <?php
        $id = $user->getUid();
        echo "<form class='form-horizontal' action='update.php?id=$id' method='post'>";
        ?>

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required">
                    <label class="control-label">Name *</label>
                    <input type="text" class="form-control" name="name" maxlength="45" value="<?= htmlspecialchars($user->getUname()) ?>" required>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required">
                    <label class="control-label"> neues Passwort</label>
                    <input type="text" class="form-control" name="passwort" value="<?= $passwort ?>">
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" name="userAktualisieren" class="btn btn-primary">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
        </form>

    </div> <!-- /container -->
</body>

</html>