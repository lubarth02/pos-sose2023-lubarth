<?php

require_once("..\..\models\User.php");
require_once("..\..\models\Article.php");

$benutzername = '';
$pwhash = '';
$salt = "PHP";

if (isset($_POST['login'])) {
    if (isset($_POST['benutzername'])) {
        $benutzername = $_POST['benutzername'];
    }
    if (isset($_POST['passwort'])) {
        $passwort = $_POST['passwort'];
        $pwhash = hash('sha512', $passwort . $salt);
    }
    if (User::isUserinDatabase($benutzername, $pwhash)) {
        setcookie("login", true, time() + 60 * 60 * 24 * 30, "/");
        header("Location: ../../index.php");
    } else {
        echo "Der User mit dem angegebenen Benutzername und dem angegebenen Passwort existiert nicht!";
    }
}

?>

<!DOCTYPE html>
<html lang="de">
<?php
include "head.php";
?>

<body>

    <?php
    $pathToUsers = "../user/index.php";
    $pathToArticles = "../article/index.php";
    $pathToIdex = "../../index.php";
    include "../helper/navbar.php";

    ?>
    <div class="container m-3">
        <form class="form-signin" action="login.php" method="post">
            <h2 class="form-signin-heading">Please login</h2>
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="form-group required">
                        <label class="control-label">Benutzername *</label>
                        <input type="text" class="form-control" name="benutzername" maxlength="45" value="">
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group required">
                        <label class="control-label">Passwort *</label>
                        <input type="password" class="form-control" name="passwort" maxlength="45" value="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <button class="btn btn-lg btn-primary btn-block" name="login" type="submit">Login</button>
                </div>
                <div class="col-sm-6 col-md-3">
                    <a class="btn btn-lg btn-primary btn-block" href="../user/register.php" type="button">registrieren</a>
                </div>
            </div>
        </form>
    </div>
</body>

</html>