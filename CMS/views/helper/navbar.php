<?php
$path = dirname(__FILE__) . '/../../models/User.php';
require_once($path);

function startsWith($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

if (isset($_POST['abmelden'])) {
    User::logout();
}

?>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= $pathToIdex?>">Awesome CMS</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="<?= $pathToArticles ?>">Beiträge</a></li>
                <li><a href="<?= $pathToUsers ?>">Benutzer</a></li>
            </ul>

            <?php
            if (User::isLoggedIn()) {
                echo "<form class='navbar-form navbar-right' action='' method='post'>";
                echo "<button type='submit' name='abmelden' class='btn btn-warning'>Abmelden</button>";
                echo "</form>";
            } else {
                echo "<a href='views\helper\login.php' class='btn btn-warning' type='button'>Login</a>";
            }
            ?>
        </div><!--/.navbar-collapse -->
    </div>
</nav>
<br><br>