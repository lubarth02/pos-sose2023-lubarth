<?php

require_once("../../models/Article.php");
require_once("../../models/User.php");
$user = new User();
$article = new Article();

if (!User::isLoggedIn()) {
    header("Location: ../helper/login.php");
} else {
    $article = new Article();

    if (isset($_POST['artikelSpeichern'])) {
        if (isset($_POST['title'])) {
            $article->setAtitle($_POST['title']);
        }
        if (isset($_POST['releasedate'])) {
            $article->setAcreationdate($_POST['releasedate']);
        }
        if (isset($_POST['owner'])) {
            $article->setUid($_POST['owner']);
        }
        if (isset($_POST['content'])) {
            $article->setAtext($_POST['content']);
        }

        if ($article->getAtitle() == null | $article->getAcreationdate() == null | $article->getAtext() == null) {
            echo "<div class='alert alert-danger'> <p> Die Daten sind ungültig!</p>";
            echo "<ul>";
            if (isset($_SESSION['title'])) {
                echo "<li>" . $_SESSION['title'] . "</li>";
            }
            if (isset($_SESSION['text'])) {
                echo "<li>" . $_SESSION['text'] . "</li>";
            }
            if (isset($_SESSION['date'])) {
                echo "<li>" . $_SESSION['date'] . "</li>";
            }
        } else {
            $article->create();
            header("Location: index.php");
        }
    }
}


?>

<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

    <?php
    $pathToArticles = "index.php";
    $pathToUsers = "../user/index.php";
    $pathToIdex = "../../index.php"; 
    include "../helper/navbar.php";
    ?>

    <div class="container">
        <div class="row">
            <h2>Beitrag erstellen</h2>
        </div>

        <form class="form-horizontal" action="create.php" method="post">

            <div class="row">
                <div class="col-md-5">
                    <div class="form-group required">
                        <label class="control-label">Titel *</label>
                        <input type="text" class="form-control" name="title" maxlength="45" value="<?= $article->getAtitle() ?>" required>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-2">
                    <div class="form-group required">
                        <label class="control-label">Freigabedatum *</label>
                        <input type="date" class="form-control" name="releasedate" value="<?= $article->getAcreationdate() ?>" required>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                    <div class="form-group required">
                        <label class="control-label">Besitzer *</label>
                        <select class="form-control" name="owner">
                            <option value="">-Besitzer auswählen-</option>
                            <option selected value="<?= htmlspecialchars($user->getUid()) ?>"> <?= $user->getUname() ?></option>
                            <?php
                            $users = User::getAll();
                            foreach ($users as $user) {
                                $userId = $user->getUid();
                                echo "<option value='$userId'>" . $user->getUname() . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group required">
                        <label class="control-label">Inhalt *</label>
                        <textarea class="form-control" name="content" rows="10" required><?= $article->getAtext() ?></textarea>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" name="artikelSpeichern" class="btn btn-success">Erstellen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->
</body>

</html>