<?php

require_once("../../models/Article.php");
require_once("../../models/User.php");

if (!User::isLoggedIn()) {
    header("Location: ../helper/login.php");
} else {

    $articles = Article::getAll();
}

if(isset($_POST['export'])){
    Article::export();
}

?>


<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

    <?php
    $pathToArticles = "index.php";
    $pathToUsers = "../user/index.php";
    $pathToIdex = "../../index.php"; 
    include "../helper/navbar.php";
    ?>

    <div class="container">
        <div class="row">
            <h2>Beiträge</h2>
        </div>
        <div class="row">
            <p>
                <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
            </p>

            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Titel</th>
                        <th>Inhalt</th>
                        <th>Besitzer</th>
                        <th>Freigabedatum</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($articles as $article) {
                        echo "<tr>";
                        echo "<td>" . $article->getAtitle() .  "</td>";
                        echo "<td>" . $article->getAtext() .  "</td>";
                        $userid = $article->getUid();
                        $user = User::get($userid);
                        echo "<td>" . $user->getUname() .  "</td>";
                        echo "<td>" . $article->getAcreationdate() .  "</td>";
                        $id = $article->getAid();
                        echo " <td><a class='btn btn-info' href='view.php?id=$id'><span class='glyphicon glyphicon-eye-open'></span></a>&nbsp;
                    <a class='btn btn-primary' href='update.php?id=$id'><span class='glyphicon glyphicon-pencil'></span></a>&nbsp;
                    <a class='btn btn-danger' href='delete.php?id=$id'><span class='glyphicon glyphicon-remove'></span></a></td>";
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
            <form action="index.php" name="fromExport" method="post">
                <button type="submit" name="export" class="btn btn-success">Beiträge in CSV exportieren </button>
            </form>
        </div>
    </div> <!-- /container -->
</body>

</html>