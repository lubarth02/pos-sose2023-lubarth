<?php

require_once("../../models/Article.php");
require_once("../../models/User.php");

if (!User::isLoggedIn()) {
    header("Location: ../helper/login.php");
} else {
    $id = '';
    $article = '';
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $article = Article::get($id);
    } else {
        header('Location: index.php');
    }
}
?>

<!DOCTYPE html>
<html lang="de">
<?php
$pathToArticles = "index.php";
$pathToUsers = "../user/index.php";
$pathToIdex = "../../index.php"; 
include "../helper/head.php";
?>

<body>

    <?php
    include "../helper/navbar.php";
    ?>

    <div class="container">
        <h2>Beitrag anzeigen</h2>

        <p>
            <?php
            echo "<a class='btn btn-primary' href='update.php?id=$id'>Aktualisieren</a>";
            echo "<a class='btn btn-danger' href='delete.php?id=$id'>Löschen</a>";
            echo "<a class='btn btn-default' href='index.php'>Zurück</a>";
            ?>
        </p>

        <table class="table table-striped table-bordered detail-view">
            <tbody>
                <tr>
                    <th>Titel</th>
                    <?php echo "<td>" . $article->getAtitle() . "</td>"; ?>
                </tr>
                <tr>
                    <th>Freigabedatum</th>
                    <?php echo "<td>" . $article->getAcreationdate() . "</td>"; ?>
                </tr>
                <tr>
                    <th>Besitzer</th>
                    <?php
                    $user = User::get($article->getUid());
                    echo "<td>" . $user->getUname() . "</td>"; ?>
                </tr>
                <tr>
                    <th>Inhalt</th>
                    <?php echo "<td>" . $article->getAtext() . "</td>"; ?>
                </tr>
            </tbody>
        </table>
    </div> <!-- /container -->
</body>

</html>