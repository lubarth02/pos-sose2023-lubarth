<?php
require_once("../../models/Article.php");
require_once("../../models/User.php");

$article = new Article();
$user = new User();

if (isset($_GET['id'])) {
    $article = Article::get($_GET['id']);
    $user = User::get($article->getUid());
}
if (isset($_POST['artikelAktualisieren'])) {
    if (isset($_POST['title'])) {
        $article->setAtitle($_POST['title']);
    }
    if (isset($_POST['releasedate'])) {
        $article->setAcreationdate($_POST['releasedate']);
    }
    if (isset($_POST['owner'])) {
        $article->setUid($_POST['owner']);
    }
    if (isset($_POST['content'])) {
        $article->setAtext($_POST['content']);
    }

    if ($article->getAtext() == null | $article->getAcreationdate() == null | $article->getAtext() == null) {
        echo "<div class='alert alert-danger'> <p> Die Daten sind ungültig!</p>";
        echo "<ul>";
        if (isset($_SESSION['title'])) {
            echo "<li>" . $_SESSION['title'] . "</li>";
        }
        if (isset($_SESSION['text'])) {
            echo "<li>" . $_SESSION['text'] . "</li>";
        }
        if (isset($_SESSION['date'])) {
            echo "<li>" . $_SESSION['date'] . "</li>";
        }
    } else {
        header("Location: index.php");
        $article->update();
    }
}
?>

<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

    <?php
    $pathToArticles = "index.php";
    $pathToUsers = "../user/index.php";
    $pathToIdex = "../../index.php"; 
    include "../helper/navbar.php";
    ?>

    <div class="container">
        <div class="row">
            <h2>Beitrag bearbeiten</h2>
        </div>

        <?php
        $id = $article->getAid();
        echo "<form class='form-horizontal' action='update.php?id=$id' method='post'>";
        ?>

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required">
                    <label class="control-label">Titel *</label>
                    <input type="text" class="form-control" name="title" maxlength="45" value="<?= htmlspecialchars($article->getAtitle()) ?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required">
                    <label class="control-label">Freigabedatum *</label>
                    <input type="date" class="form-control" name="releasedate" value="<?= htmlspecialchars(date('Y-m-d', strtotime($article->getAcreationdate())))  ?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="form-group required">
                    <label class="control-label">Besitzer *</label>
                    <select class="form-control" name="owner">
                        <option value="">-Besitzer auswählen-</option>
                        <option selected value="<?= htmlspecialchars($user->getUid()) ?>"> <?= $user->getUname() ?></option>
                        <?php
                        $users = User::getAll();
                        foreach ($users as $user) {
                            $userId = $user->getUid();
                            echo "<option value='$userId'>" . $user->getUname() . "</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group required">
                    <label class="control-label">Inhalt *</label>
                    <textarea class="form-control" name="content" rows="10"><?= $article->getAtext() ?></textarea>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" name="artikelAktualisieren" class="btn btn-primary">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
        </form>

    </div> <!-- /container -->
</body>

</html>