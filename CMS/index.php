<?php
require_once("models/Article.php");
require_once("models/User.php");

if (!User::isLoggedIn()) {
    header("Location: views/helper/login.php");
} else {

    $users = User::getAll();
}
?>
<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <title>Awesome CMS</title>

    <link rel="shortcut icon" href="css/favicon.ico" type="image/x-icon">
    <link rel="icon" href="css/favicon.ico" type="image/x-icon">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
    <?php
    $pathToUsers = "views/user/index.php";
    $pathToArticles = "views/article/index.php";
    $pathToIdex = "index.php"; 
    include "views/helper/navbar.php";
    ?>
    <div class="jumbotron">
        <div class="container">
            <h1>Hello Awesome CMS!</h1>
        </div>
    </div>
    <?php
    require_once("models/Article.php");
    ?>
    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <?php
            $articles = Article::getAll();
            foreach ($articles as $article) {
                echo "<div class='col-md-4'>";
                echo "<h2>" . $article->getAtitle() . "</h2>";
                echo "<p>" . $article->getAtext() . "</p>";
                $id = $article->getAid();
                echo "<p><a class='btn btn-default' href='views/article/view.php?id=$id' role='button'>View details &raquo;</a></p>";
                echo "</div>";
            }
            ?>
        </div>

        <hr>

        <footer>
            <p>&copy; 2017 Company, Inc.</p>
        </footer>
    </div> <!-- /container -->

</body>

</html>