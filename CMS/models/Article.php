<?php
require_once("DatabaseObject.php");
session_start();
class Article implements DatabaseObject
{
    private $aid;
    private $atext;
    private $atitle;
    private $acreationdate;
    private $uid;

    public function __construct($aid = -1, $atitle = '', $atext = '', $acreationdate = '', $uid = -1)
    {
        $this->setAid($aid);
        $this->setAtitle($atitle);
        $this->setAtext($atext);
        $this->setAcreationdate($acreationdate);
        $this->setUid($uid);
    }

    /**
     * Die Funktion liefert die Aktikelnummer zurück
     * @return int $aid die ID des Artikels
     */
    public function getAid()
    {
        return $this->aid;
    }

    /**
     * Die Funktion setzt die ArtikelID
     * @param int $aid
     */
    public function setAid($aid)
    {
        $this->aid = $aid;
    }
    /**
     * Die Funktion liefert den Titel des Artikels zurück
     * @return string $atext der Text des Artikels
     */
    public function getAtitle()
    {
        return $this->atitle;
    }

    /**
     * Die Funktion prüft den Text des Artikels und setzt ihn, wenn die Prüfung erfolgreich war
     * @param string $atext
     */
    public function setAtitle($atitle)
    {
        unset($_SESSION['title']);
        if (strlen($atitle) == 0) {
            $_SESSION['title'] = "Der Titel darf nicht leer sein";
        } else if (strlen($atitle) > 512) {
            $_SESSION['title'] = "Der Titel darf nicht länger als 512 Zeichen sein";
        } else {
            $this->atitle = $atitle;
        }
    }

    /**
     * Die Funktion liefert des Text des Artikels zurück
     * @return string $atext der Text des Artikels
     */
    public function getAtext()
    {
        return $this->atext;
    }

    /**
     * Die Funktion setzt den Text des Artikels
     * @param string $atext der Text des Artikels
     */
    public function setAtext($atext)
    {
        unset($_SESSION['text']);
        if (isset($atext) == 0) {
            $_SESSION['text'] = "Der Text darf nicht leer sein";
        } else {
            $this->atext = $atext;
        }
    }

    /**
     * Die Funktion liefert das Erstellungsdatum des Artikels zurück
     * @return string $acreationdate das Erstellungsdatum
     */
    public function getAcreationdate()
    {
        return $this->acreationdate;
    }

    /**
     * Die Funktion setzt das Erstelldatum
     * @param string $acreationdate das Erstellungsdatum
     */
    public function setAcreationdate($acreationdate)
    {
        unset($_SESSION['date']);
        if (strlen($acreationdate) == 0) {
            $_SESSION['date'] = "Das Erstellungsdatum darf nicht leer sein";
        } else {
            $this->acreationdate = $acreationdate;
        }
    }

    /**
     * Die Funktion liefert die UserID zurück
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Die Funktion setzt die UserID
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Die Funktion speichert einen neuen Artikel in die Datenbank
     * @return int die ID des Artikels
     */
    public function create()
    {
        $conn = Database::connect();
        $stmt = $conn->prepare("INSERT INTO `cms_db`.`t_artikel` (`atitle`, `atext`, `acreationdate`, `uid`) VALUES (:title, :text, :date, :uid)");
        $stmt->bindParam(':title', $this->atitle);
        $stmt->bindParam(':text', $this->atext);
        $stmt->bindParam(':date', $this->acreationdate);
        $stmt->bindParam(':uid', $this->uid);
        $stmt->execute();
        $id = $conn->lastInsertId();
        Database::disconnect();
        $this->setAid($id);
        return $id;
    }
    /**
     * Die Funktion aktualisiert eine Artikel
     * @return bool true wenn das Update erfolgreich war ansonsten false
     */
    public function update()
    {
        $conn = Database::connect();
        $stmt = $conn->prepare("UPDATE `cms_db`.`t_artikel` SET `atitle` = :title, `atext` = :text, `acreationdate` = :date, `uid` = :uid WHERE (`aid` = :id)");
        $stmt->bindParam(':title', $this->atitle);
        $stmt->bindParam(':text', $this->atext);
        $stmt->bindParam(':date', $this->acreationdate);
        $stmt->bindParam(':id', $this->aid);
        $stmt->bindParam(':uid', $this->uid);
        $stmt->execute();
        $numRows = $stmt->rowCount();
        Database::disconnect();
        if ($numRows == 1) { // Datensatz wurde eingefügt
            return true;
        } else {
            return false;
        }
    }
    /**
     * Die Funktion liefert eine Artikel mit einer bestimmten ID aus der Datenbank zurück
     * @param int $id die ID
     * @return Article $article der gefundene Artikel
     */
    public static function get($id)
    {
        $conn = Database::connect();
        $stmt = $conn->prepare("SELECT * FROM `cms_db`.`t_artikel` WHERE `aid` = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $erg = $stmt->fetch(PDO::FETCH_ASSOC);
        $article = new Article($erg['aid'], $erg['atitle'], $erg['atext'], $erg['acreationdate'], $erg['uid']);
        Database::disconnect();
        return $article;
    }

    /**
     * Die Funktion liefert alle Artikel aus der Datenbank zurück
     * @return array die gefundenen Artikel als Array of Article
     */
    public static function getAll()
    {
        $articles = array();
        $conn = Database::connect();
        $stmt = $conn->query("SELECT * FROM `cms_db`.`t_artikel`");
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $article = new Article($row['aid'], $row['atitle'], $row['atext'], $row['acreationdate'], $row['uid']);
            array_push($articles, $article);
        }
        Database::disconnect();
        return $articles;
    }
    /**
     * Diese Funktion löscht einen Article aus der Datenbank
     * @param int $id
     */
    public static function delete($id)
    {
        $conn = Database::connect();
        $stmt = $conn->prepare("DELETE FROM `cms_db`.`t_artikel` WHERE `aid` = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        Database::disconnect();
    }

    /**
     * Die Funktion exportiert die Beiträge als CSV Datei 
     */
    public static function export()
    {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=data.csv');
        $output = fopen("php://output", "w");
        ob_end_clean();
        fputcsv($output, array('aid', 'atitle', 'atext', 'acreationdate', 'uid'));
        $conn = Database::connect();
        $stmt = $conn->query("SELECT * FROM `cms_db`.`t_artikel`");
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            fputcsv($output, $row);
        }
        fclose($output);
        Database::disconnect();
        exit(); // ansonsten steht der HTML Code der Datei wo die Funktion aufgerufen wird, auch in der csv Datei
    }
}
