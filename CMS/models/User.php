<?php
require_once("DatabaseObject.php");
//session_start();
class User implements DatabaseObject
{
    private $uid;
    private $uname;
    private $upwhash;

    public function __construct($uid = -1, $uname = '', $upwhash = '')
    {
        $this->setUid($uid);
        $this->setUname($uname);
        $this->setUpwhash($upwhash);
    }

    /**
     * die Funktion liefert die ID des Benutzers zurück
     * @return int $uid die BenutzerID
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Die Funktion setzt die ID des Benutzers
     * @param int $uid die ID des Benutzers
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * Die Funktion liefert den Namen des Benutzers zurück
     * @return string $uname der Name des Benutzers
     */
    public function getUname()
    {
        return $this->uname;
    }

    /**
     * Die Funktion setzt den Benutzernamen
     * @param string $uname der Benutzername
     */
    public function setUname($uname)
    {
        unset($_SESSION['uname']);
        if (strlen($uname) == 0) {
            $_SESSION['uname'] = "Der Benutzername darf nicht leer sein";
        } else {
            $this->uname = $uname;
        }
    }

    /**
     * Die Funktion liefert den Hash des Passworts vom User
     * @return string $upwhash der Hash des Passworts des Benutzers
     */
    public function getUpwhash()
    {
        return $this->upwhash;
    }

    /**
     * Die Funktion setzt den Hash des Passworts des Benutzers
     * @param string $upwhash der Hash des Passworts
     */
    public function setUpwhash($upwhash)
    {
        unset($_SESSION['upwhash']);
        if (strlen($upwhash) == 0) {
            $_SESSION['upwhash'] = "Der Passowrthash darf nicht leer sein";
        } else {
            $this->upwhash = $upwhash;
        }
    }
    /**
     * Die Funktion erstellt einen neuen User in der Datenbank.
     * @return int $id die ID des erstellten Benutzers
     */
    public function create()
    {
        $conn = Database::connect();
        $stmt = $conn->prepare("INSERT INTO `cms_db`.`t_users` (`uname`, `upwhash`) VALUES (:name, :pwhash)");
        $stmt->bindParam(':name', $this->uname);
        $stmt->bindParam(':pwhash', $this->upwhash);
        $stmt->execute();
        $id = $conn->lastInsertId();
        $this->setUid($id);
        Database::connect();
        return $id;
    }
    /**
     * Die Funktion aktualisiert einen Benutzer in der Datenbank
     */
    public function update()
    {
        $conn = Database::connect();
        $stmt = $conn->prepare("UPDATE `cms_db`.`t_users` SET `uname` = :name, `upwhash` = :pwhash WHERE (`uid` = :id)");
        $stmt->bindParam(':name', $this->uname);
        $stmt->bindParam(':pwhash', $this->upwhash);
        $stmt->bindParam(':id', $this->uid);
        $stmt->execute();
        Database::disconnect();
    }
    /**
     * Die Funktion liefert einen Benutzer mit einer ID aus der Datenbank zurück
     * @param int $id die ID des Benutzers der aktualisiert werden soll
     * @return User den gefundenen Benutzer
     */
    public static function get($id)
    {
        $conn = Database::connect();
        $stmt = $conn->prepare("SELECT * FROM cms_db.t_users WHERE uid = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $erg = $stmt->fetch(PDO::FETCH_ASSOC);
        $user = new User($erg['uid'], $erg['uname'], $erg['upwhash']);
        Database::disconnect();
        return $user;
    }
    /**
     * Die Funktion liefert alle Benutzer aus der Datenbank
     * @return array die gefundenen Benutzer als Array of Benutzer
     */
    public static function getAll()
    {
        $users = array();
        $conn = Database::connect();
        $stmt = $conn->query("SELECT * FROM cms_db.t_users");
        while ($row = $stmt->fetch()) {
            $user = new User($row['uid'], $row['uname'], $row['upwhash']);
            array_push($users, $user);
        }
        return $users;
    }
    /**
     * Die Funktion löscht einen Benutzer aus der Datenbank
     * @param int $id die ID des Benutzers
     */
    public static function delete($id)
    {
        $conn = Database::connect();
        $stmt = $conn->prepare("DELETE FROM `cms_db`.`t_users` WHERE (`uid` = :id)");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        Database::disconnect();
    }

    /**
     * Die Funktion prüft, ob der Benutzer mit den angegebenen Logindaten vorhanden ist
     * @param string $uname der Benutzername
     * @param string $upwhash der Hash des Passworts
     * @return bool true wenn der Benutzer existiert ansonsten false
     */
    public static function isUserinDatabase($uname, $upwhash)
    {
        $conn = Database::connect();
        $stmt = $conn->prepare("SELECT * FROM `cms_db`.`t_users` WHERE (`uname` = :name) AND (`upwhash` = :hash)");
        $stmt->bindParam(':name', $uname);
        $stmt->bindParam(':hash', $upwhash);
        $stmt->execute();
        $anzahlRows = $stmt->rowCount();
        if ($anzahlRows == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Die Funktion überprüft, ob der Benutzer eingelogt ist
     * @return bool true wenn der Benutzer eingeloggt ist ansonsten false
     */
    public static function isLoggedIn()
    {
        if (isset($_COOKIE['login'])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Die Funktion setzt das Login Cookie auf false um den Benutzer auszuloggen 
     */
    public static function logout()
    {
        if (isset($_COOKIE['login'])) {
            setcookie("login", false, time() + 60 * 60 * 24 * 30, "/");
            header("Location: ../../index.php");
        }
    }
}
