<?php
require_once('RESTController.php');
require_once('models/Credentials.php');

class CredentialsRESTController extends RESTController{

    public function handleRequest()
    {
        switch ($this->method)
        {
            case 'GET':
                $this->handleGETRequest();
                break;
            case 'POST': 
                $this->handlePOSTRequest();
                break;
            case 'PUT':
                $this->handlePUTRequest();
                break;
            case 'DELETE': 
                $this->handleDELETERequest();
                break;
            default:
                $this->response('CredentialsRESTController: Method not allowed', 405);
                break;

        }
    }

    /**
     * Behandelt alle GET-Requests auf den Endpoint /credentials/
     * Alle Credentials: GET api.php?r=credentials
     * Eine Zeile: GET api.php?r=credentials/25 -> args[0] = 25
     * Suche GET api.php?r=credentials/search/domain.at -> verb=search, args[0] = domain.at
     */
    public function handleGETRequest()
    {
        //Wenn kein Verb und keine Argumente-> Einfach alle Credentials auslesen
        if ($this->verb == null && sizeof($this->args) == 0){
            $model = Credentials::getAll();
            $this->response($model);
        }
        //Wenn ein Argument vorhanden ist
        elseif ($this->verb == null && sizeof($this->args) == 1) {
            $model  = Credentials::get($this->args[0]);
            $this->response($model);
        }
        //Wenn ein Verb zur Suche vorhanden ist
        elseif ($this->verb == 'suche' && sizeof($this->args) == 1) {
            $model = Credentials::getAll($this->args[0]);
            $this->response($model);
        }
        else{
            $this->response("Bad request", 400);
        }
    }


    /**
     * Behandelt alle POST-Requests auf den Endpoint /credentials/
     */
    public function handlePOSTRequest()
    {
        $model = new Credentials();
        $model->setName($this->getDataOrNull('name'));
        $model->setDomain($this->getDataOrNull('domain'));
        $model->setCmsUsername($this->getDataOrNull('cms_username'));
        $model->setCmsPassword($this->getDataOrNull('cms_password'));

        if ($model->save()) {
            $this->response("OK", 201);
        } else {
            $this->response($model->getErrors(), 400);
        }
    }


     /**
     * Updates Credentials.
     */
    private function handlePUTRequest()
    {
        //Wenn kein Verb übergeben wurde und genau ein Argument (die ID) vorliegt
        if ($this->verb == null && sizeof($this->args) == 1) {

            //Hole das Objekt vom Model
            $model = Credentials::get($this->args[0]);

            if ($model == null) {
                $this->response("Not found", 404);
            }
            //übergib die Daten aus dem PUT-Request an das Model
            else {
                $model->setName($this->getDataOrNull('name'));
                $model->setDomain($this->getDataOrNull('domain'));
                $model->setCmsUsername($this->getDataOrNull('cms_username'));
                $model->setCmsPassword($this->getDataOrNull('cms_password'));

                //Speichere die Änderungen in der DB
                if ($model->save()) {
                    $this->response("OK");
                } else {
                    $this->response($model->getErrors(), 400);
                }
            }

        } else {
            $this->response("Not Found", 404);
        }
    }


    /**
     * Lösche Credentials: DELETE api/credentials/25 -> args[0] = 25
     */
    private function handleDELETERequest()
    {
        if ($this->verb == null && sizeof($this->args) == 1) {
            Credentials::delete($this->args[0]);
            $this->response("OK");
        } else {
            $this->response("Not Found", 404);
        }
    }

}