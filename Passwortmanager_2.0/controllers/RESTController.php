<?php

/**
 * Definiert, welche Methoden ein REST-Controller haben muss.
 * Zudem wird die URL im Konstruktor zerlegt und in die entsprechenden Datenfelder und
 * HTTP-Methode, Endpoint, HTTP-Verb und Argumente werden befüllt.
 */
abstract class RESTController{

    protected $method = '';

    protected $endpoint = '';

    protected $verb = '';

    protected $args = Array();

    protected $file = null;

    //protected $request = null;


    public function __construct()
    {
        //Setzt die HTTP-Header für die Response.
        //In diesem Fall sind alle Anfragequellen gestattet, alle HTTP-Methoden.
        //Der Anfragende hat mit JSON zu rechnen.
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
        header('Content-Type: application/json');

        //Zerlege die Anfrage, wenn es ein GET-Aufruf, in seine Bestandteile
        $this->args = isset($_GET['r']) ? explode('/', trim($_GET['r'], '/')): [];

        //Falls es nicht zu zerlegen gibt.
        if (sizeof($this->args) == 0)
        {
            throw new Exception ('Bad Request', 400);
        }

        //Der erste Bestandteil ist der Endpoint
        $this->endpoint = array_shift($this->args);
        if (array_key_exists(0, $this->args) && !is_numeric($this->args[0]))
        {
            //Der zweite Bestandteil ist das HTTP-Verb (falls es keine ID ist)
            $this->verb = array_shift($this->args);
        }

        //Hole die HTTP-Methode aus der Anfrage des Clients (GET, POST, PUT, DELETE,...)
        $this->method = $_SERVER['REQUEST_METHOD'];

        
        //Wenn die Methode ein POST-Request ist, schaue genauer hin (weil manche Server...)
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER))
        {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE')
            {
                $this->method = 'DELETE';
            }
            elseif($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT')
            {
                $this->method = 'PUT';
            }
            else{
                throw new Exception('Method not allowed2', 405);
            }
        }

        switch ($this->method)
        {
            case 'DELETE': 
            case 'POST': 
                //$this->request = $this->cleanInputs($_POST);
                $this->file = json_decode(file_get_contents('php://input'), true);
                break;
            case 'GET':
                //$this->request = $this->cleanInputs($_GET);
                break;
            case 'PUT':
                //$this->request = $this->cleanInputs($_GET);
                $this->file = json_decode(file_get_contents('php://input'), true);
                break;
            default:
                throw new Exception('Method not allowed', 405);

        }
    }//Ende Konstruktor


    protected function response($data, $status = 200)
    {
        RESTController::responseHelper($data, $status);
    }


    public static function responseHelper($data, $status)
    {
        header("HTTP/1.1 ". $status . ' ' . RESTController::requestStatus($status));
        echo json_encode($data);
    }



    private static function requestStatus($code)
    {
        $status = array(
            200 => 'OK',
            201 => 'Created',
            400 => 'Bad Request',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        );
        return key_exists($code, $status) ? $status[$code] : $status[500];
    }


    protected function getDataOrNull($field) {
        return isset($this->file[$field]) ? $this->file[$field] : null;
    }


    // private function cleanInputs($data)
    // {
    //     $clean_input = Array();
    //     if (is_array($data))
    //     {
    //         foreach ($data as $k => $v) {
    //             $clean_input[$k] = $this->cleanInputs($v);
    //         }
    //     }
    //     else{
    //         $clean_input = trim(strip_tags($data));
    //     }
    //     return $clean_input;
    // }

}