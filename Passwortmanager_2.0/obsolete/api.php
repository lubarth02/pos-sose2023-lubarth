<?php
require_once('controllers/CredentialsRESTController.php');

/*
 * Dieses Skript erledigt das Routing. Abhängig von der URL wird der richtige Teil der Anwendung geladen
 */
//Falls der GET-Parameter r existiert, soll geroutet werden
if (isset($_GET['r']))
{
    $route = explode('/', trim($_GET['r'], '/'));
    //var_dump($route);
}
else{
    //Die Standardroute, falls r nicht gesetzt ist.
    $route = ['credentials'];
}

//Hole den Namen des Controllers aus der Route
if (sizeof($route) > 0)
{
    $controller = $route[0];
}


//Wenn Credentials gefragt sind...
if ($controller == 'credentials')
{
    try {
        (new CredentialsRESTController())->handleRequest();
    }
    catch(Exception $e)
    {
        RESTController::responseHelper($e->getMessage(), $e->getCode());
    }
}
else{
    RESTController::responseHelper('REST-Controller '. $controller. ' not found', 404);
}

?>